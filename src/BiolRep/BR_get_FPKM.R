
sam.list <- c(paste("s_MCF-7_Rep", 1:3, sep=''),
              paste("s_GM12892_Rep",1:3,"_V2", sep=''),
              paste("s_H1-hESC_Rep", 1:4, sep=''))


get_FPKM <-function(x, column)
{
  track_file = paste("/scratchBulk/dob2014/SEQC/ENCODE/Mapped_Caltech/cuffLinks",
    x, "CUFFLINKS2/genes.fpkm_tracking",sep="/")
  gene_track = read.table(track_file,as.is=T,header=TRUE, stringsAsFactors=FALSE)
  
  #this need to be done as the cufflinks2_2 are not properly sorted
  gene_track.sorted <- gene_track[order(gene_track$tracking_id) ,]
  return(gene_track.sorted[,column])
} 

res = sapply(sam.list, function(x) get_FPKM(x, "FPKM"))

## rownames(res)=gene_track.sorted$gene_id
rownames(res) <- sort(get_FPKM(sam.list[1],"tracking_id"))
colnames(res) <- gsub("_V2",'',gsub('s_','',sam.list))
save(res,file=paste("../data/BiolRep/gene_FPKM_",Sys.Date(),".Rdata", sep=''))


