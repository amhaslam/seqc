
## load ERCC data
ercc <- read.table("../data/ERCC_Controls_Analysis.txt", stringsAsFactors=FALSE, header=T, sep="\t", row.names=2)

## Load DE results
## load("../data/Results_tophat2.RData")
## res.deseq <- ResDESeq[[1]]
## res.edger <- ResedgeR[[1]]
## res.limma <- Reslimma[[1]]
## res.poseq <- ResPoSeq[[1]]

## load("../data/Results_tophat2_DB_A12345_B12345_2012-09-18.Rdata")
## load("../data/Results_tophat2_DB_A12345_B12345_2012-10-15.Rdata")
## load("../data/Results_tophat2_DB_A12345_B12345_2012-11-09.Rdata")
load("../data/Results_tophat2_DB_A12345_B12345_2012-11-15.Rdata")
kNumOfMethods <- 7

res.deseq <- results.full[[1]]
res.edger <- results.full[[2]]
res.limmaQN <- results.full[[4]]
res.limmaVoom <- results.full[[5]]
res.poseq <- results.full[[3]]$res

res.bayseq <- merge(results.full[[6]]$MA,
                    results.full[[6]]$de[,c("Likelihood","FDR")],
                    by.x='row.names', by.y='row.names')
rownames(res.bayseq) <- res.bayseq[,1]
res.bayseq <- res.bayseq[,-1]

## res.noiseq <- results.full[[7]]$de

plot.dat <- list()

############
## DESeq
###########
rownames(res.deseq$de) <- res.deseq$de[,'id']
res.deseq$de <- res.deseq$de[,c('id', 'pval', 'padj', 'log2FoldChange', 'baseMeanA', 'baseMeanB')]

deseq.taq <- merge(res.deseq$de,
                   ## res.deseq$all.res,
                   ercc,
                   by.x='row.names', by.y='row.names')

## change infinite to +/- 1 from max or min
infinite.values <- deseq.taq[is.infinite(deseq.taq[,'log2FoldChange']), 'log2FoldChange'] 
infinite.values <-  sapply(infinite.values, function(x)
                           ifelse(sign(x)==1,  max(deseq.taq[is.finite(deseq.taq[,'log2FoldChange']),
                                        'log2FoldChange']) +1,
                                   min(deseq.taq[is.finite(deseq.taq[,'log2FoldChange']), 'log2FoldChange']) -1))

deseq.taq[is.infinite(deseq.taq[,'log2FoldChange']), 'log2FoldChange'] <- infinite.values
plot.dat["DESeq"] <- list(deseq.taq)


#########
## edgeR
#########
## uncomment the following if using Results_tophat2.RData
## res.edger.all <- as.data.frame(res.edger$all.res, stringsAsFactors=FALSE)
## numeric.col <- c("Pval", "FDR","logFC", "Mean_at_cond_condA","Mean_at_cond_condB")
## res.edger.all[,numeric.col] <- apply(res.edger.all[,numeric.col], 2 , function(x) as.numeric(x))

## uncomment the following if edgeR version is 2.6.12
## res.edger.all <- cbind(rownames(res.edger$de$table), res.edger$de$table[,-2],res.edger$counts$conc$conc.group)
## res.edger.all <- res.edger.all[,c(1,3,4,2,5,6)]
## colnames(res.edger.all) <- c("ID", "Pva", "FDR", "logFC", "MeanA", "MeanB")


res.edger.all <- cbind(rownames(res.edger$de$table), res.edger$de$table[,-2])
## reoder columns 
res.edger.all <- res.edger.all[,c(1,3,4,2)]
colnames(res.edger.all) <- c("ID", "Pva", "FDR", "logFC")


edger.taq <- merge(res.edger.all, ercc,
                   by.x='row.names', by.y='row.names')

plot.dat["edgeR"] <- list(edger.taq)


###############
## limma
#############
limma.taqQN <- merge(res.limmaQN$tab,
  ercc,
  by.x='row.names', by.y='row.names')

plot.dat["limmaQN"] <- list(limma.taqQN)

limma.taqVoom <- merge(res.limmaVoom$tab,
  ercc,
  by.x='row.names', by.y='row.names')

plot.dat["limmaVoom"] <- list(limma.taqVoom)


############
## PoissonSeq
############
poiss.matrix <- data.frame(tt=res.poseq$tt, pval=res.poseq$pval,
                           fdr=res.poseq$fdr, logFC=res.poseq$log.fc)
rownames(poiss.matrix) <- res.poseq$gname

poseq.dat <- merge(poiss.matrix, ercc,
                   by.x='row.names', by.y='row.names')

plot.dat["PoissonSeq"] <- list(poseq.dat)

#############
## CuffDiff
#############
## load cuffdiff
ResCuff <- read.table("../data/gene_expA12345_B12345.txt", stringsAsFactors=FALSE,
                   sep='\t', header=TRUE, row.names=2)
cuffdiff <- merge(ResCuff, ercc,
                  by.x='row.names', by.y='row.names')

## remove double.xmax values
## note that these values are machine dependend
kCuffDiffMax <- 1.79769e+308
kCuffDiffMin <- -1.79769e+308
cuffdiff <- cuffdiff[-which(cuffdiff[,10]==kCuffDiffMax) ,]
cuffdiff <- cuffdiff[-which(cuffdiff[,10]==kCuffDiffMin) ,]

plot.dat["CuffDiff"] <- list(cuffdiff)


##############
## baySeq
##############
bayseq.dat <- merge(res.bayseq, ercc,
                    by.x='row.names', by.y='row.names')
## flip sign of M values
bayseq.dat[,'M'] <- -bayseq.dat[,'M'] 

plot.dat['baySeq'] <- list(bayseq.dat)


##############
## NOISeq
##############
## noiseq.dat <- merge(res.noiseq, ercc,
##                     by.x='row.names', by.y='row.names')

## ## flip sign of M values
## noiseq.dat[,'M'] <- -noiseq.dat[,'M'] 
## plot.dat['NOISeq'] <- list(noiseq.dat)

####################
## plot correlation
####################
PlotCorrelation <- function(dat, x.index, y.index, title, col){
  plot(dat[,x.index], dat[,y.index],
       pch=19,
       col=col,
       main=title,
       xlab='ERCC mix ratio',
       ylab=paste(title, 'logFC', sep=' '))
  
}

logFC.index <- list(DESeq=5, edgeR=5, limmaQN=4,limmaVoom=4, PoissonSeq=5,
                    CuffDiff=10, baySeq=2, NOISeq=4)
## Color scheme by color pallette
## color.function <- colorRampPalette(c("orange", "gray", "red"), space='rgb')
## colr <- color.function(kNumOfMethods)
colr <- c("#A6CEE3", "#1F78B4", "#B2DF8A", "#33A02C", "#FB9A99",
          "#E31A1C", "#FDBF6F") ## "#FF7F00")

plot2file=TRUE
if(plot2file){
  pdf(paste("../results/ERCCAnalysis_", Sys.Date(), ".pdf", sep=''))
}

## plot correlations
sapply(seq(kNumOfMethods), function(i) PlotCorrelation(plot.dat[[i]],
                                           dim(plot.dat[[i]])[2],
                                           logFC.index[[i]],
                                           names(plot.dat)[i],
                                           colr[i]))

## Boxplot of log2.mix1mix2=0 q-values 
## collect zero p-values for log2.Mix.1.Mix.2.
qval.index <- list(DESeq=4, edgeR=4, limmaQN=3,limmaVoom=3, PoissonSeq=4,
                   CuffDiff=13, baySeq=5, NOISeq=6)
qval.0.mix.dat <- lapply(seq(kNumOfMethods), function(i) plot.dat[[i]][
                                                 plot.dat[[i]][,dim(plot.dat[[i]])[2]] == 0,
                                                 qval.index[[i]]
                                                 ])


names(qval.0.mix.dat) <- names(plot.dat)
## boxplot(qval.0.mix.dat, ylab='Adjusted p-values', col='bisque',
##         main='ERCC Non differentiated spike-ins adjusted p-values')

require(beeswarm)
myCol <- lapply(seq(kNumOfMethods), function(i) ifelse(qval.0.mix.dat[[i]] <=0.05, 'gray', colr[i]))
names(myCol) <- names(plot.dat)
beeswarm(qval.0.mix.dat, pch=19, add=FALSE, pwcol=myCol,
         main="ERCC Non differentiated spike-ins adjusted p-values")
abline(h=0.05, lwd=2, lty=3)


## get the fraction of false positive q-values
print(sapply(qval.0.mix.dat, function(x) table(x<0.05)))


## ROC
require(pROC)
PlotRocs <- function(i, dat, qval.index, logmix.index, color){
  outcome= rep(1, dim(dat)[1])
  outcome[dat[,logmix.index] == 0] =0
  if(i==1){
    roc <- plot.roc(outcome, dat[,qval.index],col=color,main="ROC of ERCC spike-in data")

  }else{
    roc <- lines.roc(outcome, dat[,qval.index], add=TRUE, col=color)
  }
  return(roc)
}

res <- lapply(seq(kNumOfMethods), function(i) PlotRocs(i, plot.dat[[i]],
                        qval.index[[i]],
                        dim(plot.dat[[i]])[2],
                        colr[i]))

names(res) <- names(plot.dat)
legends <- lapply(seq(kNumOfMethods), function(i) paste(names(res)[i], "AUC =", format(res[[i]]$auc, digits=3), sep=' '))
legend("bottomright", legend=legends, col=colr, lwd=2,  cex=.75)


if(plot2file){
  dev.off()
}
